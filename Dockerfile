FROM jenkins/jenkins:2.105

USER root
RUN apt-get update
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y maven nodejs ant npm build-essential libssl-dev sudo
RUN wget https://github.com/openshift/origin/releases/download/v3.6.1/openshift-origin-client-tools-v3.6.1-008f2d5-linux-64bit.tar.gz
RUN tar --strip-components 1 -xvf openshift-origin-client-tools-v3.6.1-008f2d5-linux-64bit.tar.gz -C /usr/local/bin/ openshift-origin-client-tools-v3.6.1-008f2d5-linux-64bit/oc
RUN apt-get install -y rsync
RUN apt-get install -y libnss-wrapper gettext-base

RUN chgrp -R 0 /var/jenkins_home
RUN chmod -R g+rw /var/jenkins_home
RUN find /var/jenkins_home -type d -exec chmod g+x {} +

COPY jenkins.sh /usr/local/bin/jenkins.sh
RUN chmod 755 /usr/local/bin/jenkins.sh

COPY setupAd.groovy /usr/share/jenkins/ref/init.groovy.d/setupAd.groovy
USER jenkins
COPY plugin-list /usr/share/jenkins/ref/plugins.txt
COPY passwd.template /usr/share/jenkins/ref/passwd.template
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
