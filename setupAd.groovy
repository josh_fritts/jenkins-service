import jenkins.model.*
import hudson.security.*
import hudson.security.csrf.*
import hudson.plugins.active_directory.*
   
def instance = Jenkins.getInstance()
String domain = 'corp.scholasticinc.local'
String site = 'site'
String server = '10.32.4.52:3268'
String bindName = 'service-jenkins@corp'
String bindPassword = 'd5F%pZgn'
Boolean disable_ip_in_crumb = true
Boolean anonymous_read = false

def initialSetup = "/var/jenkins_home/initialSetupComplete"
def setupFile = new File(initialSetup)

if ( setupFile.exists() ) {
    println "Initial setup already completed"
}
else {
    // Setup AD
    println "Setting up AD Realm"
    def adrealm = new ActiveDirectorySecurityRealm(domain, site, bindName, bindPassword, server)
    instance.setSecurityRealm(adrealm)
    
    println "Disabling CLI/Remoting"
    instance.getDescriptor("jenkins.CLI").get().setEnabled(false)
    
    println "Setting up login strategy"
    def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
    strategy.setAllowAnonymousRead(anonymous_read)
    instance.setAuthorizationStrategy(strategy)
    
#    // Disable IP in Crumb
#    println "Fixing Proxy support"
#    def crumb = new DefaultCrumbIssuer(disable_ip_in_crumb)
#    instance.setCrumbIssuer(crumb)

    // Save our changes
    instance.save()

    setupFile.createNewFile()
}

